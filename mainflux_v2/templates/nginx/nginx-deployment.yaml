apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.nginx.name }}
  labels:
    app: {{ .Values.nginx.name }}
    workload.user.cattle.io/workloadselector: deployment-{{.Release.Namespace}}-{{ .Values.nginx.name }}
spec:
  replicas: {{ .Values.nginx.replicaCount }}
  strategy:
    rollingUpdate:
      maxSurge: {{ .Values.upgradepolicy.strategy.rollingUpdate.maxSurge }}
      maxUnavailable: {{ .Values.upgradepolicy.strategy.rollingUpdate.maxUnavailable }}
    type: {{ .Values.upgradepolicy.strategy.type }}
  selector:
    matchLabels:
      app: {{ .Values.nginx.name }}
      workload.user.cattle.io/workloadselector: deployment-{{.Release.Namespace}}-{{ .Values.nginx.name }}
  template:
    metadata:
      labels:
        app: {{ .Values.nginx.name }}
        workload.user.cattle.io/workloadselector: deployment-{{.Release.Namespace}}-{{ .Values.nginx.name }}
    spec:
    {{- if .Values.nodeselector.enabled }}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: "{{ .Values.nodeselector.matchexpressions.key }}"
                operator: "{{ .Values.nodeselector.matchexpressions.operator }}"
                values:
                - "{{ .Values.nodeselector.matchexpressions.values }}"
    {{- end }}
      containers:
      - name: {{ .Values.nginx.name }}
        image: "{{ .Values.nginx.image.repository }}:{{ .Values.nginx.image.tag }}"
        imagePullPolicy: {{ .Values.nginx.image.pullPolicy }}
        env:
        - name: TZ
          value: "{{ .Values.environment.general.TZ }}"
        - name: MF_USERS_HTTP_PORT
          value: "{{ .Values.environment.nginx.MF_USERS_HTTP_PORT }}"
        - name: MF_THINGS_HTTP_PORT
          value: "{{ .Values.environment.nginx.MF_THINGS_HTTP_PORT }}"
        - name: MF_HTTP_ADAPTER_PORT
          value: "{{ .Values.environment.nginx.MF_HTTP_ADAPTER_PORT }}"
        - name: MF_MQTT_ADAPTER_MQTT_PORT
          value: "{{ .Values.environment.nginx.MF_MQTT_ADAPTER_MQTT_PORT }}"
        - name: MF_MQTT_ADAPTER_WS_PORT
          value: "{{ .Values.environment.nginx.MF_MQTT_ADAPTER_WS_PORT }}"
        - name: MF_UI_PORT
          value: "{{ .Values.environment.nginx.MF_UI_PORT }}"
        - name: MF_TWINS_HTTP_PORT
          value: "{{ .Values.environment.nginx.MF_TWINS_HTTP_PORT }}"
        - name: MF_OPCUA_ADAPTER_HTTP_PORT
          value: "{{ .Values.environment.nginx.MF_OPCUA_ADAPTER_HTTP_PORT }}"
        - name: MF_NGINX_SSL_PORT
          value: "{{ .Values.environment.nginx.MF_NGINX_SSL_PORT }}"
        - name: MF_NGINX_MQTTS_PORT
          value: "{{ .Values.environment.nginx.MF_NGINX_MQTTS_PORT }}"
        - name: MF_NGINX_MQTT_PORT
          value: "{{ .Values.environment.nginx.MF_NGINX_MQTT_PORT }}"
        - name: MF_NGINX_HTTP_PORT
          value: "{{ .Values.environment.nginx.MF_NGINX_HTTP_PORT }}"
        - name: MF_MQTT_CLUSTER
          value: "{{ .Values.environment.nginx.MF_MQTT_CLUSTER }}"
        - name: MF_INFLUX_READER_PORT
          value: "{{ .Values.environment.nginx.MF_INFLUX_READER_PORT }}"
        - name: MF_BOOTSTRAP_PORT
          value: "{{ .Values.environment.nginx.MF_BOOTSTRAP_PORT }}"
        - name: DOCKER_VERNEMQ_LOG__CONSOLE__LEVEL
          value: "{{ .Values.environment.nginx.DOCKER_VERNEMQ_LOG__CONSOLE__LEVEL }}"
        - name: DOCKER_VERNEMQ_ALLOW_ANONYMOUS
          value: "{{ .Values.environment.nginx.DOCKER_VERNEMQ_ALLOW_ANONYMOUS }}"
        volumeMounts:
        - mountPath: /etc/ssl/certs/dhparam.pem
          name: vol1
          subPath: dhparam.pem
        - mountPath: /etc/nginx/nginx.conf.template
          name: vol2
          subPath: nginx.conf.template
        - mountPath: /entrypoint.sh
          name: vol3
          subPath: entrypoint.sh
        - mountPath: /etc/nginx/snippets
          name: vol4
        - mountPath: /etc/nginx/authorization.js
          name: vol5
          subPath: authorization.js
        - mountPath: /etc/ssl/certs/mainflux-server.crt
          name: vol6
          subPath: mainflux-server.crt
        - mountPath: /etc/ssl/certs/ca.crt
          name: vol7
          subPath: ca.crt
        - mountPath: /etc/ssl/private/mainflux-server.key
          name: vol8
          subPath: mainflux-server.key
        args:
        - /entrypoint.sh
      imagePullSecrets:
        - name: {{ .Values.spec.imagePullSecrets }}
      volumes:
      - configMap:
          defaultMode: 0755
          name: dhparam.pem
          optional: false
        name: vol1
      - configMap:
          defaultMode: 0755
          name: nginx.conf.template
          optional: false
        name: vol2
      - configMap:
          defaultMode: 0755
          name: entrypoint.sh
          optional: false
        name: vol3
      - configMap:
          defaultMode: 0755
          name: snippets
          optional: false
        name: vol4
      - configMap:
          defaultMode: 0755
          name: authorization.js
          optional: false
        name: vol5
      - configMap:
          defaultMode: 0755
          name: mainflux-server.crt
          optional: false
        name: vol6
      - configMap:
          defaultMode: 0755
          name: ca.crt
          optional: false
        name: vol7
      - configMap:
          defaultMode: 0755
          name: mainflux-server.key
          optional: false
        name: vol8